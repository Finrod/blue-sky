import React from 'react'
import { shallow } from 'enzyme'

import App from './App'

test('App displays router components', () => {
  const component = shallow(<App />)

  expect(component).toMatchSnapshot()
})
