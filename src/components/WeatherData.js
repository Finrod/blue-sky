import React from 'react'
import PropTypes from 'prop-types'

import './WeatherData.css'

import Humidity from '../img/i_humidity.svg'
import Rain from '../img/i_rain.svg'
import Snow from '../img/i_snow.svg'
import TempMax from '../img/i_temp-max.svg'
import TempMin from '../img/i_temp-min.svg'
import Wind from '../img/i_wind.svg'

class WeatherData extends React.Component {
  getIcon() {
    if (this.props.type === 'humidity') return Humidity
    else if (this.props.type === 'rain') return Rain
    else if (this.props.type === 'snow') return Snow
    else if (this.props.type === 'temp-max') return TempMax
    else if (this.props.type === 'temp-min') return TempMin
    else if (this.props.type === 'wind') return Wind
  }

  render() {
    return (
      <div className='weather-data'>
        <img className='icon' src={this.getIcon()} alt={this.props.type} />
        {this.props.data}
      </div>
    )
  }
}

WeatherData.propTypes = {
  type: PropTypes.string.isRequired,
  data: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
}

export default WeatherData
