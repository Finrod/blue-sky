import React from 'react'
import { shallow } from 'enzyme'

import WeatherData from './WeatherData'

test('WeatherData displays a humidity icon and a value', () => {
  const component = shallow(<WeatherData type='humidity' data='90 %' />)

  expect(component).toMatchSnapshot()
})

test('WeatherData displays a rain icon and a value', () => {
  const component = shallow(<WeatherData type='rain' data='90 mm' />)

  expect(component).toMatchSnapshot()
})

test('WeatherData displays a snow icon and a value', () => {
  const component = shallow(<WeatherData type='snow' data='90 mm' />)

  expect(component).toMatchSnapshot()
})

test('WeatherData displays a temp-max icon and a value', () => {
  const component = shallow(<WeatherData type='temp-max' data='9°' />)

  expect(component).toMatchSnapshot()
})

test('WeatherData displays a temp-min icon and a value', () => {
  const component = shallow(<WeatherData type='temp-min' data='9°' />)

  expect(component).toMatchSnapshot()
})

test('WeatherData displays a wind icon and a value', () => {
  const component = shallow(<WeatherData type='wind' data='9 m/s' />)

  expect(component).toMatchSnapshot()
})
