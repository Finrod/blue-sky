import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import WeatherData from './WeatherData'

import './City.css'

class City extends React.Component {
  render() {
    const geolocIcon = this.props.city.geolocated ? (
      <i className={'material-icons geolocated'}>my_location</i>
    ) : (
      ''
    )

    return (
      <Link to={`/forecast/${this.props.city.id}`} className='city'>
        {geolocIcon}

        <img
          src={`${process.env.REACT_APP_BASENAME}/img/${this.props.city.weather[0].icon}.svg`}
          alt={this.props.city.weather[0].main}
        />

        <h1>{this.props.city.name}</h1>
        <p>{this.props.city.geolocated}</p>

        <div className='weather'>
          <WeatherData type='temp-max' data={`${this.props.city.main.temp}°`} />
          <WeatherData type='wind' data={`${this.props.city.wind.speed} m/s`} />
          <WeatherData
            type='humidity'
            data={`${this.props.city.main.humidity}%`}
          />
        </div>
      </Link>
    )
  }
}

City.propTypes = {
  city: PropTypes.object
}

export default City
