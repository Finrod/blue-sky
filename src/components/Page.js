import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'

import Header from './Header'

import './Page.css'

const HeaderWithRouter = withRouter(Header)

class Page extends React.Component {
  render() {
    return (
      <div>
        <HeaderWithRouter
          hasBackButton={this.props.header.hasBackButton}
          title={this.props.header.title}
        />

        <main className='main'>{this.props.children}</main>
      </div>
    )
  }
}

Page.propTypes = {
  header: PropTypes.shape({
    hasBackButton: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired
  }),
  children: PropTypes.element
}

export default Page
