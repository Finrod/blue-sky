import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import './Header.css'

class Header extends React.Component {
  goBack = () => {
    if (this.props.history) this.props.history.goBack()
  }

  render() {
    return (
      <nav className='header bg-black'>
        <i
          onClick={this.goBack}
          className={
            'material-icons pointer ' + (this.props.hasBackButton ? '' : 'hide')
          }
        >
          arrow_back_ios
        </i>

        <h2>{this.props.title}</h2>

        <Link to='/about'>
          <i className='material-icons'>info</i>
        </Link>
      </nav>
    )
  }
}

Header.propTypes = {
  hasBackButton: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired
}

export default Header
