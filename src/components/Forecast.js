import React from 'react'
import PropTypes from 'prop-types'
import { round } from 'lodash-es'
import format from 'date-fns/format'

import WeatherData from './WeatherData'

import './Forecast.css'

class Forecast extends React.Component {
  render() {
    const hour = format(this.props.forecast.dt_txt, 'H:mm')

    let precipitations = <WeatherData type='rain' data='0 mm' />
    if (this.props.forecast.rain && this.props.forecast.rain['3h']) {
      precipitations = (
        <WeatherData
          type='rain'
          data={round(this.props.forecast.rain['3h'], 2) + ' mm'}
        />
      )
    } else if (this.props.forecast.snow && this.props.forecast.snow['3h']) {
      precipitations = (
        <WeatherData
          type='snow'
          data={round(this.props.forecast.snow['3h'], 2) + ' mm'}
        />
      )
    }

    if (this.props.forecast.dt === 'empty') {
      return <div className='forecast' />
    } else {
      return (
        <div className='forecast'>
          <h2>{hour}</h2>

          <img
            src={`${process.env.REACT_APP_BASENAME}/img/${
              this.props.forecast.weather[0].icon
            }.svg`}
            alt={this.props.forecast.weather[0].main}
          />

          <div className='weather'>
            <WeatherData
              type='temp-max'
              data={`${this.props.forecast.main.temp}°`}
            />
            <WeatherData
              type='wind'
              data={`${this.props.forecast.wind.speed} m/s`}
            />
            <WeatherData
              type='humidity'
              data={`${this.props.forecast.main.humidity}%`}
            />
            {precipitations}
          </div>
        </div>
      )
    }
  }
}

Forecast.propTypes = {
  forecast: PropTypes.object
}

export default Forecast
