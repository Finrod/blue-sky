import React from 'react'
import { shallow } from 'enzyme'

import { round } from 'lodash-es'
import format from 'date-fns/format'

import Forecast from './Forecast'

// Mock
jest.mock('lodash-es')
round.mockImplementation(v => v)

jest.mock('date-fns/format')
format.mockImplementation(v => v)

test('Forecast displays a forecast component with rain', () => {
  const forecast = {
    dt: 1559822400,
    dt_txt: '2019-06-06 12:00:00',
    rain: {
      '3h': 5
    },
    weather: [
      {
        icon: '01d',
        main: 'sunny'
      }
    ],
    main: {
      temp: 18,
      humidity: 60
    },
    wind: {
      speed: 2
    }
  }
  const component = shallow(<Forecast forecast={forecast} />)

  expect(component).toMatchSnapshot()
})

test('Forecast displays a forecast component with snow', () => {
  const forecast = {
    dt: 1559822400,
    dt_txt: '2019-06-06 12:00:00',
    snow: {
      '3h': 5
    },
    weather: [
      {
        icon: '01d',
        main: 'sunny'
      }
    ],
    main: {
      temp: 18,
      humidity: 60
    },
    wind: {
      speed: 2
    }
  }
  const component = shallow(<Forecast forecast={forecast} />)

  expect(component).toMatchSnapshot()
})

test('Forecast displays a forecast component without rain nor snow', () => {
  const forecast = {
    dt: 1559822400,
    dt_txt: '2019-06-06 12:00:00',
    weather: [
      {
        icon: '01d',
        main: 'sunny'
      }
    ],
    main: {
      temp: 18,
      humidity: 60
    },
    wind: {
      speed: 2
    }
  }
  const component = shallow(<Forecast forecast={forecast} />)

  expect(component).toMatchSnapshot()
})

test('Forecast displays an empty forecast component', () => {
  const forecast = {
    dt: 'empty'
  }
  const component = shallow(<Forecast forecast={forecast} />)

  expect(component).toMatchSnapshot()
})
