import React from 'react'
import { shallow } from 'enzyme'

import City from './City'

test('City displays a city component', () => {
  const city = {
    id: 1,
    name: 'Rennes',
    weather: [
      {
        icon: '01d',
        main: 'sunny'
      }
    ],
    main: {
      temp: 18,
      humidity: 60
    },
    wind: {
      speed: 2
    }
  }
  const component = shallow(<City city={city} className='bg-black' />)

  expect(component).toMatchSnapshot()
})
