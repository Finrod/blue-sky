import React from 'react'
import { shallow } from 'enzyme'

import Header from './Header'

test('Header displays a header with back button', () => {
  const component = shallow(
    <Header hasBackButton={true} title='Header with back button' />
  )

  expect(component).toMatchSnapshot()
})

test('Header displays a header without back button', () => {
  const component = shallow(
    <Header hasBackButton={false} title='Header without back button' />
  )

  expect(component).toMatchSnapshot()
})
