import React from 'react'
import { shallow } from 'enzyme'

import Page from './Page'

test('Page displays a page with a header with a back button', () => {
  const header = { hasBackButton: true, title: 'Header with a back button' }
  const component = shallow(
    <Page header={header}>
      <div>Page content</div>
    </Page>
  )

  expect(component).toMatchSnapshot()
})

test('Page displays a page with a header without a back button', () => {
  const header = { hasBackButton: false, title: 'Header without a back button' }
  const component = shallow(
    <Page header={header}>
      <div>Page content</div>
    </Page>
  )

  expect(component).toMatchSnapshot()
})
