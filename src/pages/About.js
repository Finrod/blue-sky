import React from 'react'

import './About.css'

class About extends React.Component {
  render() {
    return (
      <div className='about'>
        <div className='logo'>
          <img
            src={`${process.env.REACT_APP_BASENAME}/img/01d.svg`}
            alt='Blue Sky'
          />
          <h1>Blue Sky</h1>
          <p>Version {process.env.REACT_APP_VERSION}</p>
        </div>

        <p>
          Développé par{' '}
          <a className='underline' href='https://www.sylvain-fave.com'>
            Sylvain Favé
          </a>
          .
        </p>
        <p>
          Images par{' '}
          <a
            className='underline'
            href='https://www.iconfinder.com/iconsets/the-weather-is-nice-today'
          >
            Laura Reen
          </a>{' '}
          (licence{' '}
          <a
            className='underline'
            href='https://creativecommons.org/licenses/by-nc/3.0/'
          >
            Creative Common
          </a>
          ).
        </p>
      </div>
    )
  }
}

export default About
