import React from 'react'

import { getCurrent } from '../api'

import './Cities.css'

import City from '../components/City'

class Cities extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      cities: []
    }
  }

  componentDidMount() {
    this.getWeather()
  }

  async getWeather() {
    const current = await getCurrent()
    this.setState({ cities: current.list })
  }

  render() {
    return (
      <div className='cities'>
        {this.state.cities.map((c, i) => (
          <City city={c} key={c.id} />
        ))}
      </div>
    )
  }
}

export default Cities
