import React from 'react'
import { shallow } from 'enzyme'

import About from './About'

test('About displays an about page', () => {
  const component = shallow(<About />)

  expect(component).toMatchSnapshot()
})
