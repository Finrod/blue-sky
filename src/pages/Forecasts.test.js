import React from 'react'
import { shallow } from 'enzyme'

import Forecasts from './Forecasts'

import { getForecast } from '../api'

// Mock
jest.mock('../api')
const forecast = {
  list: [
    ['2019/06/06', [{ id: 1 }, { id: 2 }, { id: 3 }]],
    ['2019/06/07', [{ id: 11 }, { id: 12 }, { id: 13 }, { id: 14 }]]
  ]
}
getForecast.mockResolvedValue(forecast)

test('Forecasts displays a list of forecasts page', () => {
  const routerParams = { params: { id: 1 } }
  const component = shallow(<Forecasts match={routerParams} />)

  expect(getForecast).toHaveBeenCalled()
  component.setState({ forecasts: forecast.list })

  expect(component).toMatchSnapshot()
})
