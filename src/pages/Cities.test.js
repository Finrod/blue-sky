import React from 'react'
import { shallow } from 'enzyme'

import Cities from './Cities'

import { getCurrent } from '../api'

// Mock
jest.mock('../api')
const current = {
  list: [{ id: 1 }, { id: 2 }]
}
getCurrent.mockResolvedValue(current)

test('Cities displays a list of cities page', () => {
  const component = shallow(<Cities />)

  expect(getCurrent).toHaveBeenCalled()
  component.setState({ cities: current.list })

  expect(component).toMatchSnapshot()
})
