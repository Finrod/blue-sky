import React from 'react'
import { upperFirst } from 'lodash-es'
import format from 'date-fns/format'
import frLocale from 'date-fns/locale/fr'

import { getForecast } from '../api'

import Forecast from '../components/Forecast'

import './Forecasts.css'

class Forecasts extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      forecasts: []
    }
  }

  componentDidMount() {
    this.getForecast()
  }

  async getForecast() {
    const forecast = await getForecast(this.props.match.params.id)
    this.setState({ forecasts: forecast.list })
  }

  formatDate(date) {
    return upperFirst(format(date, 'dddd D MMMM', { locale: frLocale }))
  }

  render() {
    return (
      <div>
        {this.state.forecasts.map((g, j) => (
          <div
            className={'forecast-day ' + (j % 2 ? 'bg-gray' : 'bg-black')}
            key={g[0]}
          >
            <h2>{this.formatDate(g[0])}</h2>

            <div className='forecasts'>
              {[0, 1, 2, 3].map(i => (
                // Display always 4 elements
                <Forecast
                  key={`${g[0]}-${i}`}
                  forecast={g[1][i] || { dt: 'empty' }}
                />
              ))}
            </div>
          </div>
        ))}
      </div>
    )
  }
}

export default Forecasts
