import axios from 'axios'
import cache from './cache'
import clientHttp from './clientHttp'

// Mock
jest.mock('axios')
axios.get.mockImplementation(v => Promise.resolve({ data: 'result' }))

jest.mock('./cache')
cache.get.mockImplementation(v => v)
cache.set.mockImplementation(v => v)

test('clientHttp.get does not make a request if cache is valid', async () => {
  cache.isValid.mockReturnValue(true)
  await clientHttp.get('testUrl', 'cacheKey')

  expect(cache.get).toHaveBeenCalled()
  expect(axios.get).not.toHaveBeenCalled()
})

test('clientHttp.get save to cache and make a request if cache is not valid', async () => {
  cache.isValid.mockReturnValue(false)
  await clientHttp.get('testUrl', 'cacheKey')

  expect(axios.get).toHaveBeenCalled()
  expect(cache.set).toHaveBeenCalled()
})
