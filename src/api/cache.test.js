import cache from './cache'

test('cache.set calls localStorage.setItem', () => {
  cache.set('test', 'savedToLocalStorage')

  expect(localStorage.setItem).toHaveBeenCalled()
})

test('cache.get calls localStorage.getItem', () => {
  localStorage.getItem.mockImplementation(v => '{ "test": "test" }')
  cache.get('test')

  expect(localStorage.getItem).toHaveBeenCalled()
})

test('cache.isValid returns false if no item is stored', () => {
  localStorage.getItem.mockImplementation(v => undefined)

  const isValid = cache.isValid('test')

  expect(isValid).toBeFalsy()
})

test('cache.isValid returns false if an item is stored from more than 10 minutes', () => {
  const longTimeAgo = new Date('2000/01/01').getTime()
  localStorage.getItem.mockImplementation(v => `{ "date": ${longTimeAgo} }`)

  const isValid = cache.isValid('test')

  expect(isValid).toBeFalsy()
})

test('cache.isValid returns true if an item is stored from less than 10 minutes', () => {
  const now = Date.now()
  localStorage.getItem.mockImplementation(v => `{ "date": ${now} }`)

  const isValid = cache.isValid('test')

  expect(isValid).toBeTruthy()
})
