import axios from 'axios'
import cache from './cache'

async function get(url, cacheKey) {
  if (cache.isValid(cacheKey)) return cache.get(cacheKey)

  try {
    const response = await axios.get(url)

    if (response) {
      cache.set(cacheKey, response.data)
      return Promise.resolve(response.data)
    } else {
      console.error(`Empty response from ${cacheKey} request.`)
    }
  } catch (e) {
    console.error(`Error from ${cacheKey} request.`, e)
  }
}

export { get }

export default {
  get
}
