import api from './index'
import clientHttp from './clientHttp'

// Mock
jest.mock('./clientHttp')

test('api.getCurrent returns sorted data with geolocated property', async () => {
  const unsortedData = {
    cnt: 5,
    list: [
      { id: 2988507 },
      { id: 3017624 },
      { id: 3007842 },
      { id: 3030949 },
      { id: 2983990 }
    ]
  }
  const sortedData = {
    cnt: 5,
    list: [
      { id: 2983990, geolocated: false },
      { id: 3007842, geolocated: false },
      { id: 3017624, geolocated: false },
      { id: 2988507, geolocated: false },
      { id: 3030949, geolocated: false }
    ]
  }
  clientHttp.get.mockImplementation(v => Promise.resolve(unsortedData))
  const result = await api.getCurrent()

  expect(result).toEqual(sortedData)
})
