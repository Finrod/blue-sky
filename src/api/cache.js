import differenceInMinutes from 'date-fns/difference_in_minutes'

function set(key, data) {
  localStorage.setItem(key, JSON.stringify({ date: Date.now(), data }))
}

function get(key) {
  return JSON.parse(localStorage.getItem(key)).data
}

// Cache is valid 10 minutes
function isValid(key) {
  const cachedData = localStorage.getItem(key)

  // No data
  if (!cachedData) return false

  // Check 10 minutes validity
  const cachedDate = JSON.parse(cachedData).date
  return differenceInMinutes(new Date(), new Date(cachedDate)) <= 10
}

export default {
  set,
  get,
  isValid
}
