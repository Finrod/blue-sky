import format from 'date-fns/format'
import clientHttp from './clientHttp'
import geolocation from '../utils/geolocation'
import { api, cities } from '../config'
import { find, filter, groupBy, toPairs, orderBy } from 'lodash-es'

async function getCurrent() {
  const cc = await getCurrentCities()
  const lc = await getCurrentLocal()

  const data = [...lc, ...cc]

  return Promise.resolve({
    cnt: data.length,
    list: data
  })
}

async function getCurrentCities() {
  const cacheKey = 'current-cities'
  const url =
    api.baseUrl +
    'group?id=' +
    cities.join(',') +
    '&units=metric&appid=' +
    api.key

  const data = await clientHttp.get(url, cacheKey)

  // Sort
  const sortedData = []
  for (let i = 0; i < cities.length; i++) {
    const city = find(data.list, c => c.id === cities[i])
    city.geolocated = false
    sortedData.push(city)
  }

  return sortedData
}

async function getCurrentLocal() {
  const cacheKey = 'current-local'

  const posResp = await geolocation.getLocation()
  if (posResp.result) {
    const lat = posResp.position.coords.latitude
    const long = posResp.position.coords.longitude
    const url = `${api.baseUrl}weather?lat=${lat}&lon=${long}&units=metric&appid=${api.key}`

    const data = await clientHttp.get(url, cacheKey)
    data.geolocated = true
    return [data]
  } else {
    return []
  }
}

async function getForecast(cityCode) {
  const cacheKey = 'forecast-' + cityCode
  const url =
    api.baseUrl + 'forecast?id=' + cityCode + '&units=metric&appid=' + api.key

  const data = await clientHttp.get(url, cacheKey)

  // Remove night hours
  const dayForecast = filter(
    data.list,
    f =>
      f.dt_txt.indexOf('00:00:00') === -1 &&
      f.dt_txt.indexOf('03:00:00') === -1 &&
      f.dt_txt.indexOf('06:00:00') === -1 &&
      f.dt_txt.indexOf('21:00:00') === -1
  )
  // Group by day
  dayForecast.map(f => {
    f.d = format(f.dt_txt, 'YYYY/MM/DD')
    return f
  })
  const groupedForecast = orderBy(
    toPairs(groupBy(dayForecast, 'd')),
    g => g[0],
    ['asc']
  )

  return Promise.resolve({
    city: data.city,
    cnt: dayForecast.length,
    list: groupedForecast
  })
}

export { getCurrent, getForecast }

export default {
  getCurrent,
  getForecast
}
