const api = {
  baseUrl: 'https://api.openweathermap.org/data/2.5/',
  key: 'cfe9d0f4c55b00113f33f401d031a82b'
}

const cities = [
  // Rennes
  2983990,
  // Landivisiau
  3007842,
  // Mousterlin (Fouesnant)
  3017624,
  // Paris
  2988507,
  // Bourg-Saint-Maurice
  3030949
]

export { api, cities }

export default {
  api,
  cities
}
