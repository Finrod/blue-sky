import React from 'react'
import { upperFirst } from 'lodash-es'
import format from 'date-fns/format'
import frLocale from 'date-fns/locale/fr'

import Cities from '../pages/Cities'
import Forecasts from '../pages/Forecasts'
import About from '../pages/About'

const routes = [
  {
    path: '/',
    exact: true,
    component: props => <Cities {...props} />,
    header: {
      title: upperFirst(
        format(Date.now(), 'dddd D MMMM', { locale: frLocale })
      ),
      hasBackButton: false
    }
  },
  {
    path: '/forecast/:id',
    exact: false,
    component: props => <Forecasts {...props} />,
    header: {
      title: 'Prévisions',
      hasBackButton: true
    }
  },
  {
    path: '/about',
    exact: false,
    component: props => <About {...props} />,
    header: {
      title: 'About',
      hasBackButton: true
    }
  }
]

export default routes
