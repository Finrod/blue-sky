import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import routes from './config/routes'

import Page from './components/Page'

class App extends React.Component {
  render() {
    return (
      <Router basename={process.env.REACT_APP_BASENAME}>
        {routes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            render={props => (
              <Page header={route.header}>{route.component(props)}</Page>
            )}
          />
        ))}
      </Router>
    )
  }
}

export default App
