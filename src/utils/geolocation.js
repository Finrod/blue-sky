function getLocation() {
  return new Promise((resolve, reject) => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        position => resolve({ result: true, position }),
        error => resolve(handleGeolocationErrors(error))
      )
    } else {
      resolve({ result: false, msg: 'Geo Location not supported by browser' })
    }
  })
}

function handleGeolocationErrors(error) {
  let msg = 'An unknown error occurred.'
  switch (error.code) {
    case error.PERMISSION_DENIED:
      msg = 'User denied the request for Geolocation.'
      break
    case error.POSITION_UNAVAILABLE:
      msg = 'Location information is unavailable.'
      break
    case error.TIMEOUT:
      msg = 'The request to get user location timed out.'
      break
    case error.UNKNOWN_ERROR:
    default:
      msg = 'An unknown error occurred.'
      break
  }

  return { result: false, msg }
}

export default { getLocation }
